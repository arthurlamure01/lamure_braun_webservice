**Install nodeJS**

https://nodejs.org/en/ 

**Install nodemon**


> npm install -g nodemon 


enter in command line:

`nodemon -v then nodejs -v`
to see if well installed



install sequelize to create the DB:
(use sudo if doesn't work)

`npm install -g sequelize-cli`



In the project file:

`npm install`

`sequelize init`


**Parametrage**

`sequelize model:create --attributes "name:string firstname:string email:string password:string solde:double" --name User`

`sequelize model:create --attributes "CardId:string UserId:integer duplicate:integer" --name UserCard`

`sequelize model:create --attributes "UserId:integer User2Id:integer status:string" --name Friend`

`sequelize model:create --attributes "UserId:integer User2Id:integer cardUserWant:string cardUserGive:string status:string" --name ExchangeCard`


Ensuite aller dans le dossier migrations/create-user.js et ajouter a solde la valeur suivante:

    DefaultValue: 0.0
    
Puis la valeur suivante au champs email, password: 

    allowNull: false
    
    
Aller dans le dossier migrations/create-user-card.js remplacer idUSERS par UserId et rajouter les champs: 

    allowNull: false,
    references: {
      model: 'Users',
      key: 'id'
    } 
        
Rajouter au champs duplicate:

    DefaultValue: 1
        

Rajouter dans migrations/create-friends.js pour les attributs UserId et User2Id les champs:

    allowNull: false,
    references: {
      model: 'Users',
      key: 'id'
    }
        
Rajouter à  l'attribue status: 

     defaultValue: 'pending' 
     
    

Rajouter dans migrations/create-ExchangeCard.js pour les attributs UserId et User2Id, les champs:

    allowNull: false,
    references: {
      model: 'Users',
      key: 'id'
    }
    
Ajouter également au champ cardUserWant et cardUserGive, les champs:

    allowNull: false
        
Puis ajouter a l'attribue status un champ

    defaultValue: 'pending' 
    
    

Rendez vous dans le fichier models/user.js pour y ajouter:

`models.User.hasMany(models.UserCard)`

`models.User.belongsToMany(models.User, {as: 'User2', through: models.Friend})`

`models.User.belongsToMany(models.User, { as: 'User', through: models.ExchangeCard })`

Puis dans le fichier models/userCard.js pour y ajouter:

`models.UserCard.belongsTo(models.User, {foreignKey: { allowNull: false }})`




Installer MAMP => preferences => ports: click on set MySql port to 80.... click ok and then launch server.

ensuite dans un terminal entrer la commande:
    `cd ~`
    
Si vous n'avez jamais setup votre mdp mysql
(le password demandé la premiere fois est root (normalement))

`    /Application/MAMP/Library/bin/mysqladmin -u root -p password <newPassword>`

Puis

`    /Application/MAMP/Library/bin/mysql -u root -p`


dans l'invite mysql entrer la commande:

   ` >create database <nom database>; ` *<nom database> que vous trouverez dans le fichier config/config.json dans les champs "database":
Le faire 3 fois pour chaque base ( dev, test, prod)

`>exit` (pour sortir de mysql)

Pour finir placer vous dans le dossier de votre projet et rentrer la commande

`sequelize db:migrate`

Command to rebuild BD if modif:

`Sequelize db:drop`

`Sequelize db:create`

`Sequelize db:migrate`




**Pour lancer le projet une fois la configuration faites :**

`nodemon start`

