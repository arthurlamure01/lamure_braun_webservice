//Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '0brNN49SFlEwJ2brMPhUInKjEooYdoboG9cbqttJ';

//Exports functions
module.exports = {
    generateTokenForUser: function(userData){
        console.log('generate token')
        return jwt.sign({
                userId: userData.id
            }, JWT_SIGN_SECRET,
            {
                expiresIn: "24 hours"
            });
    },
    parseAuthorization: function(authorization){
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    },
    getUserId: function(authorization){
        return new Promise((resolve) => {
            module.exports.checkAuthorization(authorization)
                .then((res) => {
                    console.log('id: ', res)
                    if(res != null){
                        resolve(res.userId);
                    }
                })
                .catch(() => {
                    resolve(-1)
                });
        });
    },
    checkAuthorization: (authorization) => {
        return  new Promise((resolve, reject) => {
            let token = module.exports.parseAuthorization(authorization);
            console.log("token: ", token);
            if(token != null) {
                try {
                    let jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                    if(jwtToken != null && jwtToken.userId != null){
                        userId = jwtToken.userId;
                        resolve({userId: userId, token: token})
                    }
                    reject("Reject when verify");
                }
                catch (err) {
                    reject(err)
                }
            }
            reject("No token sended");
        });
    }
}
