'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    firstname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    solde: DataTypes.DOUBLE
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    models.User.hasMany(models.UserCard, {onDelete: 'cascade', hooks: true})
    models.User.belongsToMany(models.User, {as: 'User2', through: models.Friend, onDelete: 'cascade', hooks: true})
    models.User.belongsToMany(models.User, { as: 'User', through: models.ExchangeCard, onDelete: 'cascade', hooks: true })
  };
  return User;
};