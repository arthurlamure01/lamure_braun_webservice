'use strict';
module.exports = (sequelize, DataTypes) => {
  const ExchangeCard = sequelize.define('ExchangeCard', {
    UserId: DataTypes.INTEGER,
    User2Id: DataTypes.INTEGER,
    cardUserWant: DataTypes.STRING,
    cardUserGive: DataTypes.STRING,
    status: DataTypes.STRING
  }, {});
  ExchangeCard.associate = function(models) {
    // associations can be defined here
  };
  return ExchangeCard;
};