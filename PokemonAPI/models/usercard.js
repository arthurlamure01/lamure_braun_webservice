'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserCard = sequelize.define('UserCard', {
    CardId: DataTypes.STRING,
    UserId: DataTypes.INTEGER,
    duplicate: DataTypes.INTEGER
  }, {});
  UserCard.associate = function(models) {
    // associations can be defined here
    models.UserCard.belongsTo(models.User, {foreignKey: { allowNull: false }, onDelete: 'cascade', hooks: true})

  };
  return UserCard;
};