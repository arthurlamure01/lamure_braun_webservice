'use strict';
module.exports = (sequelize, DataTypes) => {
  const Friend = sequelize.define('Friend', {
    UserId: DataTypes.INTEGER,
    User2Id: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {});
  Friend.associate = function(models) {
    // associations can be defined here
  };
  return Friend;
};