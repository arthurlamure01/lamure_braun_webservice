 // Imports
const express = require('express');
const bodyParser = require('body-parser');
const apiRouter = require('./apiRouter').router; //précise qu'on veut l'objet router dans ce document.

//Instantiate server
const server = express();


//body-parser (récuperer arguments et parametres fournis dans le body d'une requête http
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());


//Configures Routes
server.get('/', function(request, response){
    response.setHeader('content-type', 'text/html'); //Json apres
    response.status(200).send('<h1> Test serveur </h1>');
});
server.use('/api/', apiRouter);



//Launch server
server.listen(8080, function(){
    console.log('le serveur à démarrer');
});
