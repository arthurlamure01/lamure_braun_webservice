const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');


const usersCardsCtrl = require('./UserCardsCtrl');


//routes
module.exports = {
    exchangeCard: (user1Id, user2Id, user1CardGiven, user2CardGiven) => {
        return new Promise((resolve, reject) => {
            console.log('User1Id: ', user1Id)
            console.log('User2Id: ', user2Id)
            usersCardsCtrl.looseOneCard(user1Id, user1CardGiven)
                .then(() => {
                    usersCardsCtrl.looseOneCard(user2Id, user2CardGiven)
                        .then(() => {
                            usersCardsCtrl.obtainOneCard(user1Id, user2CardGiven)
                                .then(() => {
                                    usersCardsCtrl.obtainOneCard(user2Id, user1CardGiven)
                                        .then(() => {
                                            resolve('exchange successful')
                                        })
                                        .catch((err) => {
                                            //on enleve la carte que l'user1 a obtenu et rend les carte aux deux joueurs
                                            usersCardsCtrl.looseOneCard(user1Id, user2CardGiven)
                                                .then(() => {
                                                    usersCardsCtrl.obtainOneCard(user1Id, user1CardGiven)
                                                        .then(() => {
                                                            usersCardsCtrl.obtainOneCard(user2Id, user2CardGiven).then(() => {
                                                                reject(err)
                                                            })
                                                                .catch(() => {
                                                                    reject(err)
                                                                })
                                                        })
                                                        .catch(() => {
                                                            reject(err)
                                                        })
                                                })
                                                .catch(() => {
                                                    reject(err)
                                                })
                                        })
                                })
                                .catch((err) => {
                                    //on redonne leurs cartes au deux joueurs
                                    usersCardsCtrl.obtainOneCard(user1Id, user1CardGiven)
                                        .then(() => {
                                            usersCardsCtrl.obtainOneCard(user2Id, user2CardGiven).then(() => {
                                                reject(err)
                                            })
                                                .catch(() => {
                                                    reject(err)
                                                })
                                        })
                                        .catch(() => {
                                            reject(err)
                                        })
                                })

                        })
                        .catch((err) => {
                            //on redonne la carte au premier joueur
                            usersCardsCtrl.obtainOneCard(user1Id, user1CardGiven)
                                .then(() => {
                                    reject(err);
                                })
                                .catch(() => {
                                    reject(err);
                                })
                        })
                })
                .catch((err) => {
                    reject(err);
                })
        })
    },
    deleteProposition: (userId, user2Id, cardUser1Give, cardUser2give) => {
        return new Promise((resolve, reject) => {
            models.ExchangeCard.findOne({
                where: {
                    UserId: userId,
                    User2Id: user2Id,
                    cardUserGive: cardUser1Give,
                    cardUserWant: cardUser2give
                }
            })
                .then((propositionFound) => {
                    if (propositionFound) {
                        propositionFound.destroy()
                            .then(() => {
                                resolve('[deleteProposition] proposition well destroyed')
                            })
                            .catch(() => {
                                reject('[deleteProposition] cannot destroy proposition')
                            })
                    }
                    else {
                        reject('[deleteProposition] no proposition found')
                    }
                })
                .catch(() => {
                    reject('[deleteProposition] error while founding this proposition')
                });
        });
    },

    declineProposition: (req, res) => {    // on decline une proposition que l'on NOUS a fait (User2Id = currUser)
        const headerAuth = req.header('token');
        const user2Id = req.body.User2Id
        const cardUserWant = req.body.cardUserWant
        const cardUserGive = req.body.cardUserGive

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                module.exports.deleteProposition(user2Id, userId, cardUserGive, cardUserWant)
                    .then(() => res.status(200).json('proposition well decline'))
                    .catch((err) => res.status(500).json({'error': err}))

            })
    },
    acceptProposition: (req, res) => {    // acceptProposition() on accept une proposition que l'on NOUS a fait
        const headerAuth = req.header('token');
        const user2Id = req.body.User2Id
        const cardUserWant = req.body.cardUserWant
        const cardUserGive = req.body.cardUserGive

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                models.ExchangeCard.findOne({
                    where:{
                        UserId: user2Id,
                        User2Id: userId,
                        cardUserWant: cardUserGive,
                        cardUserGive: cardUserWant
                    }
                })
                    .then((propositionFound) => {
                        if (propositionFound) {
                            if (propositionFound.dataValues.status == 'pending') {
                               module.exports.exchangeCard(userId, user2Id, cardUserGive, cardUserWant)
                                   .then((exchangeResult) => {
                                       propositionFound.destroy()
                                           .then(() => { res.status(200).json(exchangeResult).json('proposition well destroy')})
                                           .catch(() => res.status(500).json({'error': 'cannot destroy the proposition'}))
                                   })
                                   .catch((err) => {
                                       console.log('err: ', err)
                                       return res.status(500).json({'error': 'cannot finalize the exchange'})
                                   })
                            }
                        }
                    })
                    .catch(() => res.status(500).json({'error': 'cannot find this exchange'}))
            })

    },
    cancelProposition: (req, res) => {     // cancelPropostion() on annule une proposition que l'ON  fait
        const headerAuth = req.header('token');
        const user2Id = req.body.User2Id
        const cardUserGive = req.body.cardUserGive
        const cardUserWant = req.body.cardUserWant

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                module.exports.deleteProposition(userId, user2Id, cardUserGive, cardUserWant)
                    .then(() => res.status(200).json('proposition well cancel'))
                    .catch((err) => res.status(500).json({'error': err}))

            })
    },
    createNewProposition: (req, res)  => {
        const headerAuth = req.header('token');
        const user2Id = req.body.User2Id
        const cardUserWant = req.body.cardUserWant
        const cardUserGive = req.body.cardUserGive

        // TODO on verifie qu'une proposition n'a pas déjà été faites

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                // l'User1 à accès seulement aux cartes que l'User2 à donc pas besoin de vérifier si il a la carte
                models.ExchangeCard.create({
                    UserId: userId,
                    User2Id: user2Id,
                    cardUserWant: cardUserWant,
                    cardUserGive: cardUserGive
                })
                    .then((propositionCreated) => {
                        return res.status(201).json({
                            'User1Id': propositionCreated.UserId,
                            'User2Id': propositionCreated.User2Id,
                            'cardUser1want' : propositionCreated.cardUserWant,
                            'cardUser1give' : propositionCreated.cardUserGive,
                            'status' : propositionCreated.status
                        });
                    })
                    .catch((err) => {
                        console.log(err)
                        return res.status(500).json({'error': 'cannot create this exchange'})
                    })

            })

    },
    getPropositions: (req, res) => {    //  getAllProposition() return all ExchangeCard where User2Id = currentUser
        const headerAuth = req.header('token');

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                models.ExchangeCard.findAll({
                    where:{
                        User2Id: userId
                    }
                })
                    .then((propositionsFound) => {
                        if (propositionsFound != null && propositionsFound.length > 0) {
                            let propositions = [];
                            let idsCardUserGive = []
                            let idsCardUserWant = []
                            propositionsFound.forEach((proposition) => {
                                propositions.push(proposition.dataValues);
                                idsCardUserGive.push(proposition.dataValues.cardUserGive)
                                idsCardUserWant.push(proposition.dataValues.cardUserWant)
                            });
                            usersCardsCtrl.convertIdsToCards(idsCardUserGive)
                                .then((cardsUserGive) => {
                                    usersCardsCtrl.convertIdsToCards(idsCardUserWant)
                                        .then((cardsUserWant) => {
                                            if (cardsUserGive && cardsUserWant) {
                                                let indxPropositions = 0
                                                propositions.forEach((proposition) => {
                                                    proposition.cardUserGive = cardsUserGive.cards[indxPropositions]
                                                    proposition.cardUserWant = cardsUserWant.cards[indxPropositions]
                                                    indxPropositions ++
                                                })
                                                return res.status(200).json({propositions: propositions});
                                            }
                                            else return res.status(400).json({'error': 'no cards found'})
                                        })
                                        .catch((err) => {
                                            console.log(err)
                                            return res.status(500).json({error: err})
                                        })
                                })
                                .catch((err) => {
                                    console.log(err)
                                    return res.status(500).json({error: err})
                                })
                        }
                        else return res.status(400).json({'error': 'no propositions found'})
                    })
                    .catch(() => res.status(500).json({'error': 'error when tried to find propositions made for you'}))
            })

    },
    getMyPropositions: (req, res) => {    //  return all ExchangeCard where UserId = currentUser
        const headerAuth = req.header('token');
        const userIdParam = req.params.id

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0 || userId != userIdParam)
                    return res.status(500).json({'error': 'no user found'})

                models.ExchangeCard.findAll({
                    where:{
                        UserId: userId,
                    }
                })
                    .then((propositionsFound) => {
                        if (propositionsFound != null && propositionsFound.length > 0) {
                            let propositions = [];
                            let idsCardUserGive = []
                            let idsCardUserWant = []
                            propositionsFound.forEach((proposition) => {
                                propositions.push(proposition.dataValues);
                                idsCardUserGive.push(proposition.dataValues.cardUserGive)
                                idsCardUserWant.push(proposition.dataValues.cardUserWant)
                            });
                            usersCardsCtrl.convertIdsToCards(idsCardUserGive)
                                .then((cardsUserGive) => {
                                    usersCardsCtrl.convertIdsToCards(idsCardUserWant)
                                        .then((cardsUserWant) => {
                                            if (cardsUserGive && cardsUserWant) {
                                                let indxPropositions = 0
                                                propositions.forEach((proposition) => {
                                                    proposition.cardUserGive = cardsUserGive.cards[indxPropositions]
                                                    proposition.cardUserWant = cardsUserWant.cards[indxPropositions]
                                                    indxPropositions ++
                                                })
                                                return res.status(200).json({propositions: propositions});
                                            }
                                            else return res.status(400).json({'error': 'no cards found'})
                                        })
                                        .catch((err) => {
                                            console.log(err)
                                            return res.status(500).json({error: err})
                                        })
                                })
                                .catch((err) => {
                                    console.log(err)
                                    return res.status(500).json({error: err})
                                })
                        }
                        else return res.status(400).json({'error': 'no propositions found'})
                    })
                    .catch(() => res.status(500).json({'error': 'error when tried to find your propositions'}))
            })

    }
};
