const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');
const userCardCtrl = require('./UserCardsCtrl');

//Routes
module.exports = {
    deleteFriend: (userId, user2Id) => {
        return new Promise((resolve, reject) => {
            models.Friend.findOne({
                where: {UserId: userId, User2Id: user2Id}
            })
                .then((friendRelation) => {
                    friendRelation.destroy()
                        .then((itemDeleted) => {
                            resolve(itemDeleted)
                        })
                        .catch(() => {
                            reject()
                        })
                })
                .catch((err) => {
                    reject(err)
                })
        });
    },
    addFriend: (userId, user2Id) => {
        return new Promise((resolve, reject) => {
            models.Friend.create({
                UserId: userId,
                User2Id: user2Id
            })
                .then((friendRelation) => {
                    resolve(friendRelation)
                })
                .catch((err) => {
                    console.log('[addFriend] error: ', err)
                    reject("Can't add friend")
                })
        })
    },
    convertIdToUser: (id) => {
        return new Promise((resolve) => {
            console.log(id)
            models.User.findOne({
                where:{
                    id: id
                }
            })
                .then((userFound) => {
                    console.log('something')
                    userFound !=  null ? resolve({userID: userFound.dataValues.id, email: userFound.dataValues.email, name: userFound.dataValues.name, firstname: userFound.dataValues.firstname}) : resolve(null);
                })
                .catch(() => {
                    console.log('something error')

                    resolve(null);
                })
        })
    },

    sendFriendRequest: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.friendId

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if (userId < 0)
                return res.status(400);
            if (userId === user2Id)
                return res.status(500).json({'error': 'you cannot be you own friend'});

            models.Friend.findOne({
                where: {
                    userId: userId,
                    user2Id: user2Id
                }
            })
                .then((friendFound) => {
                    if (friendFound) { //requete deja envoyé
                        if(friendFound.dataValues.status === 'pending')
                            return res.status(500).json({'error': 'request for friend already send'})
                        else
                            return res.status(500).json({'error': 'friend already added'})
                    }
                    else {
                        module.exports.addFriend(userId, user2Id)
                            .then((friendAdded) => {
                                return res.status(200).json({userID: friendAdded.dataValues.User2Id})
                            })
                    }
                })
                .catch(() => res.status(500).json({'error': 'error while finding friend'}))
        });
    },
    acceptFriendRequest: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.friendId

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if (userId < 0)
                return res.status(400);
            if (userId === user2Id)
                return res.status(500).json({'error': 'you cannot be you own friend'});
            models.Friend.findOne({
                where: {
                    userId: user2Id,
                    user2Id: userId
                }
            })
                .then((friendFound) => {
                    if (friendFound) { //requete deja envoyé
                        if(friendFound.dataValues.status === 'pending') {
                            friendFound.update({
                                status: 'validate'
                            })
                                .then(() => {
                                    module.exports.addFriend(userId, user2Id)
                                        .then((friendRequest) => {
                                            if (friendRequest) {
                                               friendRequest.update({
                                                   status: 'validate'
                                               })
                                                   .then(() => res.status(200).json({state: 'request well accepted'}))
                                                   .catch(() => res.stats(500).json({'error': 'cannot terminate processus to add friends'}))
                                            }
                                            else return res.status(500).json({'error': 'when trying to accept friendrequest'})
                                        })
                                        .catch(() => res.status(500).json({'error': 'error when trying to create friend'}))
                                })
                                .catch(() => res.status(500).json({'error': 'error when trying to accept friend request'}))
                        }
                        else return res.status(500).json({'error': 'friend already add'})
                    }
                    else return res.status(500).json({'error': 'error no friend request found'})
                })
                .catch(() => res.status(500).json({'error': 'error while finding friend'}))
        });
    },
    declineFriendRequest: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.friendId

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if (userId < 0)
                return res.status(400);
            if (userId === user2Id)
                return res.status(500).json({'error': 'you cannot be you own friend'});

            models.Friend.findOne({
                where: {
                    userId: user2Id,
                    user2Id: userId
                }
            })
                .then((friendFound) => {
                    if (friendFound) {
                       friendFound.destroy()
                           .then(() => res.status(200).json({state: 'request well cancel'}))
                           .catch(() => res.status(500).json({state: 'error when trying to cancel request'}))
                    }
                    else return res.status(500).json({'error': "cannot refuse request which doesn't exist"})
                })
                .catch(() => res.status(500).json({'error': 'error while finding friend'}))
        });
    },

    addFriendsRelation: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.friendId

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if(userId < 0)
                return res.status(400);
            if (userId === user2Id)
                return res.status(500).json({'error': 'you cannot be you own friend'});

            models.Friend.findAll({  //check if already exist
                where: {
                    $or: [
                        {
                            userId: userId,
                            user2Id: user2Id
                        },
                        {
                            userId: user2Id,
                            user2Id: userId
                        }
                    ]
                }
            })
                .then((friendsRelation) => {
                    if (friendsRelation != null && friendsRelation.length === 0) {// Si pas de requete envoyé ou pas ami
                        module.exports.addFriend(userId, user2Id)
                            .then((friendRelation1) => {
                                module.exports.addFriend(user2Id, userId)
                                    .then((friendRelation2) => {
                                        let friendsRelation = {
                                            relation1: friendRelation1,
                                            relation2: friendRelation2
                                        }
                                        return res.status(202).json({ flag: 'created', friendsRelation: friendsRelation })
                                    })
                                    .catch(() =>  res.status(500).json({'error': "cannot add second friend relation"}))
                            })
                            .catch(() =>  res.status(500).json({'error': "cannot add first friend relation"}))
                    }
                    else if(friendsRelation != null && friendsRelation.length > 0) { // Si requete d'ami déjà envoyée ou ami déjà ajouté
                        friendsRelation.forEach((value) => {
                            console.log('value: ', value)
                            if (value.dataValues.status === 'pending') {
                                value.update({
                                    status: 'validate'
                                })
                                    .then((itemUpdated) => res.status(202).json({
                                        flag: 'updated',
                                        friendsRelation: itemUpdated
                                    }))
                                    .catch(() => res.status(500).json({'error': "can't update value 'Friend.status'"}))
                            } else if (value.dataValues.status === 'validate') {
                                return res.status(500).json({'error': "friend already added"})
                            }
                        })
                    }
                })
                .catch((err) => {
                    console.log('findAll ERROR: ', err)
                })
        })
    },
    deleteFriendsRelation: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.friendId

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(400);
                module.exports.deleteFriend(userId, user2Id)
                    .then((itemDeleted1) => {
                        module.exports.deleteFriend(user2Id, userId)
                            .then((itemDeleted2) => {
                                let Relation = {
                                    firstRelation: itemDeleted1,
                                    secondRelation: itemDeleted2
                                }
                                return res.status(201).json({ 'itemsDeleted': Relation });
                            })
                            .catch(() => res.status(500).json({'error': 'cannot delete second friend relation'}))
                    })
                    .catch(() => res.status(500).json({'error': 'cannot delete first friend relation'}))
            })

    },
    getFriends: (req, res) => {
        const headerAuth = req.header('token');
        console.log('test')

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                models.Friend.findAll({
                    where: {userId: userId}
                })
                    .then((friendsFounded) => {
                        let friendsId = []
                        friendsFounded.forEach((value) => {
                            if (value != null && value.User2Id != null) {
                                friendsId.push(value.User2Id);
                            }
                        })
                        let promises = [];
                        friendsId.forEach((id) => {
                            promises.push(module.exports.convertIdToUser(id))
                        });

                        Promise.all(promises)
                            .then((friends) => {
                                return res.status(200).json({friends: friends})
                            })
                            .catch(() => res.status(500).json({'error': 'error promise.all getFriends'}))

                    })
                    .catch(() => {
                        return res.status(500).json({'error': 'cannot find all friends'})
                    })
            })
            .catch(() => {
                return res.status(500).json({'error': 'error token cannot get userId'})
            })
    },
    getFriend: (req, res) => {
        const headerAuth = req.header('token');
        let user2Id = req.params.id

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                models.Friend.findOne({
                    where: {
                        userId: userId,
                        user2Id: user2Id
                    }
                })
                    .then((friendFound) => {
                        if (friendFound) {
                            module.exports.convertIdToUser(friendFound.dataValues.User2Id)
                                .then((friend) => res.status(202).json({friend: friend}))
                                .catch(() => res.status(500).json({'error': 'cannot find this user'}))
                        }
                        else return res.status(500).json({'error': 'no user found'})
                    })
                    .catch(() => res.status(500).json({'error': 'no user found'}))
            })
            .catch(() => res.status(202).json({'error': 'Token error'}))
    },
    getFriendCards: (req, res) => { // Display before exchange
        const headerAuth = req.header('token');
        let friendId = req.params.id

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if (friendId === userId){
                    return res.status(400).json({'error': 'You cannot be you own friend'})
                }
                models.Friend.findOne({
                    where: {
                        UserId: userId,
                        User2Id: friendId
                    }
                })
                    .then((friendFound) => {
                        if (friendFound != null && friendFound.dataValues.status === 'validate') {
                            userCardCtrl.getUserCardsIdsToCards(friendId)
                                .then((cards) => res.status(200).json(cards))
                                .catch((err) => res.status(500).json({'error': err}))
                        }
                        else {
                            return res.status(400).json({'error': 'No friend found or not accepted yet'})
                        }
                    })
            })
            .catch(() => {
                return res.status(500).json({'error': 'Cannot find this friend relation'})
            })
    }

}


