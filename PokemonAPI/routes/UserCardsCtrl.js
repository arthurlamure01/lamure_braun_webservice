//Imports
const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');
const cardsCtrl = require('./cardsCtrl');
const fetch = require('node-fetch');


//Routes
module.exports = {
    looseOneCard: (userId, cardId) => { // fonction call into ExchangeCardCtrl
        return new Promise((resolve, reject) => {
            models.UserCard.findOne({
                where: {
                    UserId: userId,
                    CardId: cardId
                }
            })
                .then((userCardFound) => {
                    if (userCardFound) {
                        if (userCardFound.dataValues.duplicate > 0) { // Soit on perd on carte
                            userCardFound.update({
                                duplicate: (userCardFound.dataValues.duplicate - 1)
                            })
                                .then((cardUpdated) => {
                                    if (userCardFound.dataValues.duplicate === 0) {
                                        userCardFound.destroy()
                                            .then((itemDestroy) => {
                                                resolve({itemDestroy})
                                            })
                                            .catch(() => {
                                                reject('cannot destroy this userCard')
                                            })
                                    }
                                    else
                                        resolve(cardUpdated)
                                })
                                .catch(() => {
                                    reject('cannot update value duplicate')
                                })
                        }
                        else { // soit on delete
                            userCardFound.destroy()
                                .then((itemDestroy) => {
                                    resolve(itemDestroy)
                                })
                                .catch(() => {
                                    reject('cannot destroy this userCard')
                                })
                        }
                    }
                })
                .catch((err) => {
                    reject(err)
                })
        })
    },
    obtainOneCard: (userId, cardId) => {// fonction call into ExchangeCardCtrl
        return new Promise((resolve, reject) => {

            models.UserCard.findOne({
                where: {
                    CardId: cardId,
                    UserId: userId
                }
            })
                .then((cardFound) => {
                    if (cardFound) {
                        cardFound.update({
                            duplicate: (cardFound.dataValues.duplicate + 1)
                        })
                            .then(() => {
                                resolve(cardFound);
                            })
                            .catch(() => {
                                reject('cannot update value duplicate of userCard');
                            })
                    }
                    else
                    {
                        models.UserCard.create({
                            CardId: cardId,
                            UserId: userId
                        })
                            .then((cardCreated) => {
                                resolve(cardCreated)
                            })
                            .catch(() => {
                                reject('cannot create this userCard')
                            })
                    }
                })
                .catch(() => {
                    reject('cannot find this userCard')
                })
        })
    },
    getUserCardsIdsToCards: (userId) => {
        return new Promise((resolve, reject) => {
            models.UserCard.findAll({
                where: {UserId: userId}
            })
                .then((succes) => {
                    let userCardList =  [];
                    succes.forEach((value) => {
                        userCardList.push(value.dataValues.CardId);
                    });
                    module.exports.convertIdsToCards(userCardList)
                        .then((cards) => {
                            resolve(cards);
                        })
                        .catch((err) => {
                            reject(err);
                        });
                })
                .catch((err) => {
                    reject(err);
                })
        })
    },
    convertIdsToCards: (ids) => {
        return new Promise((resolve, reject) => {
            cardsCtrl.getCardsWithId(ids)
                .then((cards) => {
                    //Filtrer données Ici si martiff veut pas tout les champs
                    resolve(cards);
                })
                .catch((err) => {
                    reject(err);
                })

        });
    },
    userObtainCard: (req, res) => {
        const headerAuth = req.header('token');
        let cardId = req.params.id

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if(userId < 0)
                return res.status(500).json({'error': 'no user found'})
            module.exports.obtainOneCard(userId, cardId)
                .then((card) => res.status(202).json({card: card}))
                .catch((err) => res.status(500).json({'error': err}))
        })
            .catch(() => res.status(500).json({'error': 'Token error'}))
    },
    getUserCards: (req, res) => {
        const headerAuth = req.header('token');

        jwtUtils.getUserId(headerAuth).then((userId) => {
            if(userId < 0)
                return res.status(500).json({'error': 'no user found'})
            module.exports.getUserCardsIdsToCards(userId)
                .then((cards) => {
                    return res.status(200).json(cards)
                })
                .catch((err) => res.status(500).json({'error': err}))
        })
    },
    deleteUserCard: (req, res) => { // TODO refaire cette fonction pour qu'elle soit appelé par deleteUserCards
        const headerAuth = req.header('token');
        let cardId = req.params.id

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if(userId < 0)
                    return res.status(500).json({'error': 'no user found'})

                console.log(userId)
                console.log(cardId)
                models.UserCard.findOne({
                    where:{
                        UserId: userId,
                        CardId: cardId
                    }
                })
                    .then((cardFounded) => {
                        cardFounded.destroy()
                            .then((itemDetroy) => res.status(200).json({item: itemDetroy}))
                            .catch(() => res.status(500).json({'error' : 'cannot destroy this userCard relation'}))
                    })
                    .catch((err) => {
                        console.log(err)
                        return res.status(500).json({'error': 'cannot find this userCard relation'})
                    })

            })
            .catch(() => res.status(500).json({'error': 'Token error'}))
    },
    pokedex: (req, res) => {
        const headerAuth = req.header('token');
        const query = req.query;

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                cardsCtrl.getPokedex(query)
                    .then((cards) => {
                        if (cards != null && cards.cards.length > 0) {
                            module.exports.getUserCardsIdsToCards(userId)
                                .then((cardsUser) => {
                                    if (cardsUser != null && cardsUser.cards.length > 0) {
                                         cards.cards.some((pokedexCard) => {
                                            cardsUser.cards.some((cardUser) => {
                                                pokedexCard.owned = pokedexCard.id === cardUser.id
                                                return pokedexCard.id === cardUser.id
                                            })
                                        })
                                        return res.status(200).json(cards)
                                    }
                                    else {
                                        console.log('no user cards: ', cardsUser);
                                        cards.cards.forEach((pokedexCard) => {
                                            pokedexCard.owned = false
                                        })
                                        return res.status(200).json(cards)
                                    }
                                })
                        }
                        else return res.status(500).json({'cards': 'empty'})
                    })
            })
    },
    deleteUserCards: (req, res) => { // TODO deleteUserCards

    }
}
