//Imports
const bcrypt = require('bcrypt');
const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');

const REGEX_EMAIL = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

//Routes
module.exports = {
    register: function(req, res){
        const email = req.body.email;
        const name = req.body.name;
        const firstname = req.body.firstname;
        const password = req.body.password;

        if (email == null || name == null || firstname == null || password == null) {
            return res.status(400).json({'error': 'missing parameters'});
        }

        if(!REGEX_EMAIL.test(email)){
            return res.status(400).json({'error': 'invalid mail format'});
        }

        models.User.findOne({
            attributes: ['email'],
            where: {email: email}
        })
            .then(function(userFound){
                if(!userFound){
                    //hash
                    bcrypt.hash(password, 5, function (err, bcryptedPassword) {
                        const newUser = models.User.create({
                            email: email,
                            name: name,
                            firstname: firstname,
                            password: bcryptedPassword
                        })
                            .then(function(newUser){
                                if (newUser) {
                                    return res.status(201).json({
                                        'userID': newUser.dataValues.id,
                                        'email': newUser.dataValues.email,
                                        'name': newUser.dataValues.name,
                                        'firstname': newUser.dataValues.firstname,
                                        'solde': newUser.dataValues.solde,
                                        'token': null
                                    });
                                }
                                else res.status(500).json({'error': 'error when try to create user'})
                            })
                            .catch(function(err){
                                return res.status(500).json({'error': 'cannot add user'});
                            })
                    })
                }
                else{
                    return res.status(409).json({'error': 'user already exist'});
                }
            })
            .catch(function(err){
                return err.status(500).json({'error': 'unable to verify user'});
            });

    },
    login: function(req,res){ //AutoLogin with jwt token
        const email = req.body.email;
        const pass = req.body.password;

        if (email == null) return res.status(400).json({'error': "email's missing"});

        if (!REGEX_EMAIL.test(email)) return res.status(400).json({'error': 'invalid mail format'});


        models.User.findOne({
            where: {email: email}
        })
            .then(function(userFound){
                if(userFound){
                    if (pass == null) return res.status(400).json({'error': "password's missing"});

                    bcrypt.compare(pass, userFound.password, function(errBcrypt, resBcrypt){
                        if(resBcrypt){
                            return res.status(200).json({
                                'userID': userFound.dataValues.id,
                                'email': userFound.dataValues.email,
                                'name': userFound.dataValues.name,
                                'firstname': userFound.dataValues.firstname,
                                'solde': userFound.dataValues.solde,
                                'token': jwtUtils.generateTokenForUser(userFound)
                            });
                        }
                        else return res.status(403).json({'error': 'invalid password'});
                    })
                }
                else return res.status(404).json({'error': 'user not exist in the BD'});
            })

    },
    auto_login: function(req,res){ //AutoLogin with jwt token
        let headerAuth = req.header('token');

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if (userId < 0)
                    return res.status(400).json({'error': 'error token verification'})

                models.User.findOne({
                    where: {id: userId}
                })
                    .then((userFound) => {
                        if (userFound) {
                            return res.status(200).json({
                                'userID': userFound.dataValues.id,
                                'email': userFound.dataValues.email,
                                'name': userFound.dataValues.name,
                                'firstname': userFound.dataValues.firstname,
                                'solde': userFound.dataValues.solde,
                                'token': headerAuth
                            });
                        }
                        else return res.status(400).json({'error': 'no user found'})
                    })
            })
    },
    getUserProfile: function(req, res){ // si error authorization undefined create authorization via l'onglet postman (possible que token ne soit plus valide)
        //get auth header

        const headerAuth = req.header('token');
        jwtUtils.getUserId(headerAuth).then((userId) => {
            if(userId < 0) //init a -1 dans l'autre fonction si jamais pas d'user rencontré
                return res.status(400).json({'error': 'invalid token'});
            console.log('userID: ', userId)
            models.User.findOne({
                attributes: ['id', 'email', 'name', 'firstname', 'solde'],
                where: {id: userId}
            })
                .then(function(user){
                    if(user){
                        return res.status(201).json({
                            userID: user.id,
                            email: user.email,
                            name: user.name,
                            firstname: user.firstname,
                            solde: user.solde,
                            token: headerAuth
                        });
                    } else {
                        return  res.status(404).json({'error': 'user not found'});
                    }
                })
                .catch(function () {
                    return res.status(500).json({'error': 'cannot fetch user'});
                })
        });
    },
    update: (req, res) => {
        const headerAuth = req.header('token');
        const email = req.body.email;
        const name = req.body.name;
        const firstname = req.body.firstname;
        const solde = req.body.solde;

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if (userId < 0)
                    return res.status(400).json({'error': 'No user found'});

                models.User.findOne({
                    where: {id: userId}
                })
                    .then((userFound) => {
                        if (userFound) {
                           userFound.update(
                               {
                                   email: email != null ? email : userFound.dataValues.email,
                                   name: name != null ? name : userFound.dataValues.name,
                                   firstname: firstname != null ? firstname : userFound.dataValues.firstname,
                                   solde: solde != null ? solde : userFound.dataValues.solde
                               }
                           )
                               .then((userUpdated) => res.status(200).json({
                                   userID: userUpdated.id,
                                   email: userUpdated.email,
                                   name: userUpdated.name,
                                   firstname: userUpdated.firstname,
                                   solde: userUpdated.solde,
                                   token: headerAuth
                               }))
                               .catch(() => res.status(500).json({'error': 'cannot update user'}))
                        }
                        else return  res.status(500).json({'error': 'no user found'})
                    })
                    .catch(() => res.status(500).json({'error': 'user not found'}))
            })
    },
    delete: (req, res) => {
        const headerAuth = req.header('token');

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if (userId < 0)
                    return res.status(400).json({'error': 'No user found'});
                models.User.findOne({
                    where: {id: userId}
                })
                    .then((userFound) => {
                        if (userFound) {
                            userFound.destroy()
                                .then(() => res.status(200).json('User Well deleted'))
                                .catch((err) => {
                                    console.log(err)
                                    return res.status(500).json({'error' : 'Cannot destroy the user'})
                                })
                        }
                        else return  res.status(500).json({'error': 'no user found'})
                    })
                    .catch(() => res.status(500).json({'error': 'user not found'}))
            })
    }, // TODO add onDelete: 'cascade' in all relation models.js

    // TODO deleteAccount

    getUsers: (req, res) => {
        const headerAuth = req.header('token');

        jwtUtils.getUserId(headerAuth)
            .then((userId) => {
                if (userId < 0)
                    return res.status(400).json({'error': 'No user found'});

                models.User.findAll({
                    where: {id: {$not: userId}}
                })
                    .then((usersFound) => {
                        if (usersFound) {
                            let tempUser = [];
                            usersFound.forEach((user) => {
                                tempUser.push({userID: user.dataValues.id, email: user.dataValues.email, name: user.dataValues.name, firstname: user.dataValues.firstname})
                            })
                            return res.status(200).json({users: tempUser})
                        }
                        else return res.status(500).json({'error': 'no users found'});
                    })
                    .catch(() => res.status(500).json({'error': 'user not found'}))
            })
    }
}
;
