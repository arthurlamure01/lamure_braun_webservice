const fetch = require('node-fetch');
const axios = require('axios').default;
const jwtUtils = require('../utils/jwt.utils');


//routes
module.exports = {
    getPokedex: (query) => {
        return new Promise((resolve, reject) => {
            //Build the route
            let API_URL = "https://api.pokemontcg.io/v1/cards";

            if (query != null || query !== "") {
                API_URL += '?';
                for (const key in query) {
                    API_URL += key + '=' + query[key] + '&' ;
                }
                API_URL = API_URL.substring(0, API_URL.length - 1);
            }

            fetch(API_URL)
                .then(res => res.json())
                .then(json => {
                    resolve(json);
                });
        })
    },
    getCards: async (req, res) => { //permet de récup seulement une carte par son nom
        const query = req.query;

        //Build the route
        let API_URL = "https://api.pokemontcg.io/v1/cards";
        if (query != null || query !== "") {
            API_URL += '?';
            for (const key in query) {
                API_URL += key + '=' + query[key] + '&' ;
            }
            API_URL = API_URL.substring(0, API_URL.length - 1);
        }

        const response = await fetch(API_URL);
        const json = await response.json();

        return res.status(201).json(json);
    },
    getCardsWithId: async (ids) => {
        if(ids.length > 0) {
            let API_URL = "https://api.pokemontcg.io/v1/cards?id=";

            ids.forEach((id) => {
                API_URL += id + '|'
            });
            const response = await fetch(API_URL);
            return await response.json();
        }
        else return {cards: []}

    },
    getCard: async (req, res) => {
        if(req.params != null && req.params.id != null){
            const pokemonId =  req.params.id;
            const API_URL = "https://api.pokemontcg.io/v1/cards/" + pokemonId;

            console.log('[Card] apiurl: ', API_URL);
            const response = await fetch(API_URL);
            const json = await response.json();

            return res.status(201).json(json);
        }
        // TODO verifier tout les return / resolve / reject du code
    },
    getSets: async (req, res) => {
        const query = req.query;
        let API_URL = "https://api.pokemontcg.io/v1/sets";
        let nbPage = null;

        if(query != null && query.code != null){
            const temp_response = await fetch(API_URL);
            const temp_json = await temp_response.json();
            for(const key of temp_json.sets){
                if(key.code == query.code){
                    nbPage = Math.trunc((key.totalCards / 100) + 1);
                }
            }
            API_URL = "https://api.pokemontcg.io/v1/cards?setCode=" + query.code;

            if(query.page != null ){
                API_URL = "https://api.pokemontcg.io/v1/cards?setCode=" + query.code + "&page=" + query.page;
            }
        }

        console.log(API_URL)
        const final_response = await fetch(API_URL);
        let final_json = {nbPage: Object};
        final_json = await final_response.json();
        final_json.nbPage = nbPage;


        return res.status(201).json(final_json);
    },
    getSetsCodes: async (req, res) => {
        const API_URL = "https://api.pokemontcg.io/v1/sets";
        const sets = {codes: []};
        console.log("[codes]");
        const response = await fetch(API_URL);
        const json = await response.json();
        for(const key of json.sets){
            sets.codes.push(key.code);
        }

        return res.status(201).json(sets);
    },
    getTypes: async (req, res) => {
        const API_URL = "https://api.pokemontcg.io/v1/types";
        console.log('[types] apiurl: ', API_URL);
        const response = await fetch(API_URL);
        const json = await response.json();

        return res.status(201).json(json);
    },
    getSubTypes: async (req, res) => {
        const API_URL = "https://api.pokemontcg.io/v1/subtypes";
        console.log('[types] apiurl: ', API_URL);
        const response = await fetch(API_URL);
        const json = await response.json();

        return res.status(201).json(json);
    },
    getSuperTypes: async (req, res) => {
        const API_URL = "https://api.pokemontcg.io/v1/supertypes";
        console.log('[types] apiurl: ', API_URL);
        const response = await fetch(API_URL);
        const json = await response.json();

        return res.status(201).json(json);
    }

};
