//Imports
const express = require('express');
const usersCtrl = require('./routes/usersCtrl');
const cardsCtrl = require('./routes/cardsCtrl');
const usersCardsCtrl = require('./routes/UserCardsCtrl');
const friendsCtrls = require('./routes/friendsCtrls');
const exchangeCtrl = require('./routes/exchangeCardsCtrl');

//Router
exports.router = (function () {
    var apiRouter = express.Router();

    //Users routes
    apiRouter.route('/user/register/').post(usersCtrl.register);
    apiRouter.route('/user/login/').post(usersCtrl.login);
    apiRouter.route('/user/auto_login/').post(usersCtrl.auto_login);
    apiRouter.route('/user/me/').get(usersCtrl.getUserProfile);
    apiRouter.route('/user/me/').put(usersCtrl.update);
    apiRouter.route('/user/delete').delete(usersCtrl.delete);
    apiRouter.route('/user/users').get(usersCtrl.getUsers);

    //Cards routes
    apiRouter.route('/cards').get(cardsCtrl.getCards); // query params to search cards by name / types / etc.
    apiRouter.route('/pokedex').get(usersCardsCtrl.pokedex); // query params to search cards by name / types / etc.
    apiRouter.route('/cards/:id').get(cardsCtrl.getCard);
    apiRouter.route('/sets').get(cardsCtrl.getSets);
    apiRouter.route('/sets/codes').get(cardsCtrl.getSetsCodes);
    apiRouter.route('/types').get(cardsCtrl.getTypes);
    apiRouter.route('/subtypes').get(cardsCtrl.getSubTypes);
    apiRouter.route('/supertypes').get(cardsCtrl.getSuperTypes);

    //UserCards routes
    apiRouter.route('/user/cards/:id').post(usersCardsCtrl.userObtainCard);
    apiRouter.route('/user/cards').get(usersCardsCtrl.getUserCards);
    apiRouter.route('/user/cards/:id').delete(usersCardsCtrl.deleteUserCard);

    //Friends routes
    apiRouter.route('/user/friends/:friendId').post(friendsCtrls.sendFriendRequest);
//    apiRouter.route('/user/friends/:friendId').post(friendsCtrls.addFriendsRelation);
    apiRouter.route('/user/friends').get(friendsCtrls.getFriends);
    apiRouter.route('/user/friends/:friendId/accept').post(friendsCtrls.acceptFriendRequest);
    apiRouter.route('/user/friends/:friendId/decline').post(friendsCtrls.declineFriendRequest);
    apiRouter.route('/user/friends/:id').get(friendsCtrls.getFriend);
    apiRouter.route('/user/friends/:id/cards').get(friendsCtrls.getFriendCards);

    //Exchange routes
    apiRouter.route('/user/exchange').post(exchangeCtrl.createNewProposition);
    apiRouter.route('/user/exchange/cancel').post(exchangeCtrl.cancelProposition);
    apiRouter.route('/user/exchange/decline').post(exchangeCtrl.declineProposition);
    apiRouter.route('/user/exchange/accept').post(exchangeCtrl.acceptProposition);
    apiRouter.route('/user/exchanges').get(exchangeCtrl.getPropositions);
    apiRouter.route('/user/:id/exchanges').get(exchangeCtrl.getMyPropositions);



    return apiRouter;
})(); // ATTENTION !!!! parenthèse a la fin très important
